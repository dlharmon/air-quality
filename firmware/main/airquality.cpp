#include <string.h>
#include <stdio.h>
#include "protocol_examples_common.h"
#include "sdkconfig.h"
#include "esp_event.h"
#include "esp_wifi.h"
#include "esp_wifi_default.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_netif_ip_addr.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "secrets.h"
#include "driver/i2c.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "mdns.h"
#include "netdb.h"
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include <esp_http_server.h>
#include <math.h>
#include <esp_http_client.h>
#include <esp_sntp.h>

#include "scd30.h"
#include "sgp40.h"
#include "shtc1.h"

char hostname[100];

#define EXAMPLE_WIFI_SCAN_METHOD WIFI_FAST_SCAN
//#define EXAMPLE_WIFI_SCAN_METHOD WIFI_ALL_CHANNEL_SCAN

//#define EXAMPLE_WIFI_CONNECT_AP_SORT_METHOD WIFI_CONNECT_AP_BY_SIGNAL
#define EXAMPLE_WIFI_CONNECT_AP_SORT_METHOD WIFI_CONNECT_AP_BY_SECURITY

//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_OPEN
//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WEP
//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_PSK
#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_PSK
//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_WPA2_PSK
//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_ENTERPRISE
//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA3_PSK
//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_WPA3_PSK
//#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WAPI_PSK

static int s_active_interfaces = 0;
static xSemaphoreHandle s_semph_get_ip_addrs;
static esp_netif_t *s_example_esp_netif = NULL;
static const char *TAG = "wifi_connect";
static esp_netif_t *wifi_start(void);
static void wifi_stop(void);

/**
 * @brief Checks the netif description if it contains specified prefix.
 * All netifs created withing common connect component are prefixed with the module TAG,
 * so it returns true if the specified netif is owned by this module
 */
static bool is_our_netif(const char *prefix, esp_netif_t *netif)
{
    return strncmp(prefix, esp_netif_get_desc(netif), strlen(prefix) - 1) == 0;
}

/* set up connection, Wi-Fi */
static void start(void)
{
    s_example_esp_netif = wifi_start();
    s_active_interfaces++;
    /* create semaphore if at least one interface is active */
    s_semph_get_ip_addrs = xSemaphoreCreateCounting(s_active_interfaces, 0);
}

/* tear down connection, release resources */
static void stop(void)
{
    wifi_stop();
    s_active_interfaces--;
}

static esp_ip4_addr_t s_ip_addr;

static void on_got_ip(void *arg, esp_event_base_t event_base,
                      int32_t event_id, void *event_data)
{
    ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
    if (!is_our_netif(TAG, event->esp_netif)) {
        ESP_LOGW(TAG, "Got IPv4 from another interface \"%s\": ignored", esp_netif_get_desc(event->esp_netif));
        return;
    }
    ESP_LOGI(TAG, "Got IPv4 event: Interface \"%s\" address: " IPSTR, esp_netif_get_desc(event->esp_netif), IP2STR(&event->ip_info.ip));
    memcpy(&s_ip_addr, &event->ip_info.ip, sizeof(s_ip_addr));
    xSemaphoreGive(s_semph_get_ip_addrs);
}

esp_err_t wifi_connect(void)
{
    if (s_semph_get_ip_addrs != NULL) {
        return ESP_ERR_INVALID_STATE;
    }
    start();
    ESP_ERROR_CHECK(esp_register_shutdown_handler(&stop));
    ESP_LOGI(TAG, "Waiting for IP(s)");
    for (int i = 0; i < s_active_interfaces; ++i) {
        xSemaphoreTake(s_semph_get_ip_addrs, portMAX_DELAY);
    }
    // iterate over active interfaces, and print out IPs of "our" netifs
    esp_netif_t *netif = NULL;
    esp_netif_ip_info_t ip;
    for (int i = 0; i < esp_netif_get_nr_of_ifs(); ++i) {
        netif = esp_netif_next(netif);
        if (is_our_netif(TAG, netif)) {
            ESP_LOGI(TAG, "Connected to %s", esp_netif_get_desc(netif));
            ESP_ERROR_CHECK(esp_netif_get_ip_info(netif, &ip));

            ESP_LOGI(TAG, "- IPv4 address: " IPSTR, IP2STR(&ip.ip));
        }
    }
    return ESP_OK;
}

esp_err_t example_disconnect(void)
{
        if (s_semph_get_ip_addrs == NULL) {
                return ESP_ERR_INVALID_STATE;
        }
        vSemaphoreDelete(s_semph_get_ip_addrs);
        s_semph_get_ip_addrs = NULL;
        stop();
        ESP_ERROR_CHECK(esp_unregister_shutdown_handler(&stop));
        return ESP_OK;
}

static void on_wifi_disconnect(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data)
{
    ESP_LOGI(TAG, "Wi-Fi disconnected, trying to reconnect...");
    esp_err_t err = esp_wifi_connect();
    if (err == ESP_ERR_WIFI_NOT_STARTED) {
        return;
    }
    ESP_ERROR_CHECK(err);
}

static esp_netif_t *wifi_start(void)
{
    char *desc;
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_netif_inherent_config_t esp_netif_config = ESP_NETIF_INHERENT_DEFAULT_WIFI_STA();
    // Prefix the interface description with the module TAG
    // Warning: the interface desc is used in tests to capture actual connection details (IP, gw, mask)
    asprintf(&desc, "%s: %s", TAG, esp_netif_config.if_desc);
    esp_netif_config.if_desc = desc;
    esp_netif_config.route_prio = 128;
    esp_netif_t *netif = esp_netif_create_wifi(WIFI_IF_STA, &esp_netif_config);
    free(desc);
    esp_wifi_set_default_wifi_sta_handlers();

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &on_wifi_disconnect, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_ip, NULL));

    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config{};
    strncpy((char*)wifi_config.sta.ssid, WIFI_SSID, 32);
    strncpy((char*)wifi_config.sta.password, WIFI_PASS, 32);
    wifi_config.sta.scan_method = EXAMPLE_WIFI_SCAN_METHOD;
    wifi_config.sta.sort_method = EXAMPLE_WIFI_CONNECT_AP_SORT_METHOD;
    wifi_config.sta.threshold.rssi = CONFIG_EXAMPLE_WIFI_SCAN_RSSI_THRESHOLD;
    wifi_config.sta.threshold.authmode = EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD;
    ESP_LOGI(TAG, "Connecting to %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    esp_err_t ret = tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA ,hostname);
    if(ret != ESP_OK ){
      ESP_LOGE("wifi","failed to set hostname:%d",ret);
    }
    esp_wifi_connect();
    return netif;
}

static void wifi_stop(void)
{
    esp_netif_t *wifi_netif = get_example_netif_from_desc("sta");
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &on_wifi_disconnect));
    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_ip));
    esp_err_t err = esp_wifi_stop();
    if (err == ESP_ERR_WIFI_NOT_INIT) {
        return;
    }
    ESP_ERROR_CHECK(err);
    ESP_ERROR_CHECK(esp_wifi_deinit());
    ESP_ERROR_CHECK(esp_wifi_clear_default_wifi_driver_and_handlers(wifi_netif));
    esp_netif_destroy(wifi_netif);
    s_example_esp_netif = NULL;
}

esp_netif_t *get_example_netif(void)
{
    return s_example_esp_netif;
}

esp_netif_t *get_example_netif_from_desc(const char *desc)
{
    esp_netif_t *netif = NULL;
    char *expected_desc;
    asprintf(&expected_desc, "%s: %s", TAG, desc);
    while ((netif = esp_netif_next(netif)) != NULL) {
        if (strcmp(esp_netif_get_desc(netif), expected_desc) == 0) {
            free(expected_desc);
            return netif;
        }
    }
    free(expected_desc);
    return netif;
}

static void initialise_mdns(char *hostname)
{
    ESP_ERROR_CHECK( mdns_init() );
    ESP_ERROR_CHECK( mdns_hostname_set(hostname) );
    ESP_ERROR_CHECK( mdns_instance_name_set("air quality sensor") );
    mdns_txt_item_t serviceTxtData[1] = {
        {"board", "esp32"},
    };
    ESP_ERROR_CHECK( mdns_service_add("WebServer", "_http", "_tcp", 80, serviceTxtData, 1) );
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_http", "_tcp", "path", "/index.html") );
}

static const char *TAG_HTTP = "http";

esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    /* For any other URI send 404 and close socket */
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "404");
    return ESP_FAIL;
}

class TempRHSensor {
private:
        SemaphoreHandle_t _mutex;
        void _lock() {xSemaphoreTake(_mutex, portMAX_DELAY);}
        void _unlock() {xSemaphoreGive(_mutex);}
        // must hold mutex to access these two
        uint16_t _count = 0;
        int32_t _sum_temp;
        int32_t _sum_rh;
        bool using_co2 = false;
public:
        bool valid = false;
        bool present = false;
        TempRHSensor() {}
        void update();
        void measure();
        void init();
        float temperature = 0.0;
        float rh = 0.0f;
};

void TempRHSensor::update(){
        if(!present)
                return;
        _lock();
        float inv_count = 0.001f / _count;
        temperature = _sum_temp * inv_count;
        rh = _sum_rh * inv_count;
        valid = 1;
        _count = 0;
        _sum_temp = 0;
        _sum_rh = 0;
        _unlock();
}

void TempRHSensor::init() {
        _mutex = xSemaphoreCreateMutex();
        for(int i=0; i<3; i++) {
                if (shtc1_probe() == NO_ERROR) {
                        present = true;
                        break;
                }
                printf("SHTC1 sensor probing failed\n");
                vTaskDelay(1000/portTICK_RATE_MS);
        }
        if(!present)
                return;
        shtc1_enable_low_power_mode(0);
        shtc1_measure();
}

void TempRHSensor::measure() {
        if(!present)
                return;
        int32_t temp_int, rh_int;
        int16_t err = shtc1_read(&temp_int, &rh_int);
        shtc1_measure();
        if(err == NO_ERROR) {
                _lock();
                _sum_temp += temp_int;
                _sum_rh += rh_int;
                _count ++;
                _unlock();
        }
}

TempRHSensor temp_rh;

class ParticulateSensor {
private:
        SemaphoreHandle_t _mutex;
        void _lock() {xSemaphoreTake(_mutex, portMAX_DELAY);}
        void _unlock() {xSemaphoreGive(_mutex);}
        // must hold mutex to access these two
        uint16_t _count = 0;
        uint32_t _sum[12];
        // from particles.py
        const float particle_scale[5] = {
                3.04183401e-4, 1.85120122e-3, 2.06970589e-2, 2.31400153e-1, 1.85120122e0};
public:
        const char* names[12] = {"PM1", "PM2.5", "PM10", "PM1_atmospheric", "PM2.5_atmospheric", "PM10_atmospheric", "particulate_0.3", "particulate_0.5", "particulate_1", "particulate_2.5", "particulate_5", "particulate_10"};
        bool valid = false;
        float data_averaged[12];
        float usepa_pm25;
        float usepa_pm25_alt;
        ParticulateSensor() {}
        void task();
        void init() {
                _mutex = xSemaphoreCreateMutex();
                // UART init
                uart_config_t uart_config = {
                        .baud_rate = 9600,
                        .data_bits = UART_DATA_8_BITS,
                        .parity    = UART_PARITY_DISABLE,
                        .stop_bits = UART_STOP_BITS_1,
                        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
                        .source_clk = UART_SCLK_APB,
                };
                int intr_alloc_flags = 0;
                ESP_ERROR_CHECK(uart_param_config(1, &uart_config));
                ESP_ERROR_CHECK(uart_driver_install(1, 1024, 0, 0, NULL, intr_alloc_flags));
                ESP_ERROR_CHECK(uart_set_pin(1, 5, 4, -1, -1));
        }
        void update();
};

void ParticulateSensor::update() {
        if(_count != 0) {
                _lock();
                float inv_count = 1.0f/_count;
                for(size_t i=0; i<12; i++) {
                        data_averaged[i] = _sum[i] * inv_count;
                        _sum[i] = 0;
                }
                float alt_pm25 = 0.0f;
                const float scales[3] = {10.0f, 8.0f, 4.5f};
                for(int i=0; i<3; i++) {
                        float count = data_averaged[6+i] - data_averaged[7+i];
                        alt_pm25 += scales[i]*count*particle_scale[i];
                }
                usepa_pm25 = 0.52f * 4.5f * data_averaged[4];// - 0.085f * temp_rh.rh + 5.71f;
                usepa_pm25_alt = 0.52f * alt_pm25;// - 0.085f * temp_rh.rh + 5.71f;
                valid = true;
                _count = 0;
                _unlock();
        } else {
                valid = false;
        }
}

void ParticulateSensor::task() {
        char byte = 0;
        uint32_t data_32[8];
        uint16_t *data = (uint16_t*) data_32;
        while (1) {
                // Get the framing sequence
                uart_read_bytes(1, &byte, 1, 2000 / portTICK_RATE_MS);
                if(byte != 0x42)
                        continue;
                uart_read_bytes(1, &byte, 1, 2000 / portTICK_RATE_MS);
                if(byte != 0x4D)
                        continue;
                // count, data, checksum (30 bytes)
                uint32_t buf[8];
                uart_read_bytes(1, buf, 30, 2000 / portTICK_RATE_MS);
                uint8_t *buf_8 = (uint8_t*) buf;
                uint16_t sum = 0x42 + 0x4D;
                for(size_t i=0; i<28; i++)
                        sum += buf_8[i];
                // endian swap 16 bit data
                for(size_t i=0; i<8; i++) {
                        uint32_t tmp;
                        tmp = 0xFF00FF00 & (buf[i] << 8);
                        data_32[i] = (0x00FF00FF & (buf[i] >> 8)) | tmp;
                }
                //if(data[14] == sum) {
                _lock();
                for(size_t i=0; i<12; i++)
                        _sum[i] += data[i+1];
                _count ++;
                _unlock();
        }
}

static ParticulateSensor particulate;

class CO2Sensor {
private:
        SemaphoreHandle_t _mutex;
        void _lock() {xSemaphoreTake(_mutex, portMAX_DELAY);}
        void _unlock() {xSemaphoreGive(_mutex);}
        // must hold mutex to access these two
        uint16_t _count = 0;
        float _sum_temp = 0.0f, _sum_rh = 0.0f, _sum_co2 = 0.0f;
        uint16_t _cal_request = 0;
public:
        bool present = false;
        bool valid = false;
        float value;
        float temperature;
        float rh;
        CO2Sensor() {}
        void update();
        void measure();
        void init();
        void calibrate(uint16_t val);
};

void CO2Sensor::update() {
        if(!present)
                return;
        if(_count == 0) {
                valid = 0;
                temp_rh.valid = 0;
                return;
        }
        _lock();
        float inv_count = 1.0f/_count;
        value = _sum_co2 * inv_count;
        temperature = _sum_temp * inv_count;
        rh = _sum_rh * inv_count;
        valid = true;
        _count = 0;
        _sum_temp = 0;
        _sum_rh = 0;
        _sum_co2 = 0;
        _unlock();
        temp_rh.temperature = temperature - 3.0f;
        temp_rh.rh = rh;
        temp_rh.valid = valid;
}

void CO2Sensor::calibrate(uint16_t val) {
        _lock();
        _cal_request = val;
        _unlock();
}

void CO2Sensor::measure() {
        if(!present)
                return;
        uint16_t data_ready;
        if (scd30_get_data_ready(&data_ready) != NO_ERROR) {
                printf("Error reading data_ready flag\n");
                return;
        }
        if (!data_ready)
                return;
        float co2_ppm, temperature, relative_humidity;
        int16_t err = scd30_read_measurement(&co2_ppm, &temperature, &relative_humidity);
        if (err != NO_ERROR) {
                printf("error reading measurement\n");
                return;
        }
        printf("co2: %0.2f ppm, temp: %0.2f C, RH: %0.2f %%\n",
               co2_ppm, temperature, relative_humidity);
        _lock();
        _sum_temp += temperature;
        _sum_co2 += co2_ppm;
        _sum_rh += relative_humidity;
        _count ++;
        if(_cal_request != 0) {
                scd30_set_forced_recalibration(_cal_request);
                _cal_request = 0;
        }
        _unlock();
}

void CO2Sensor::init() {
        _mutex = xSemaphoreCreateMutex();
        uint32_t interval_in_seconds = 2;
        for(int i=0; i<3; i++) {
                if (scd30_probe() == NO_ERROR) {
                        present = 1;
                        break;
                }
                printf("SCD30 sensor not found attempt %d\n", i+1);
                vTaskDelay(1000/portTICK_RATE_MS);
        }
        if(!present)
                return;
        scd30_set_measurement_interval(interval_in_seconds);
        printf("SCD30 measurement interval set\n");
        vTaskDelay(20 / portTICK_PERIOD_MS );
        printf("slept 20 ms\n");
        scd30_start_periodic_measurement(0);
        printf("start meas\n");
        scd30_set_altitude(1585); // Denver
        scd30_enable_automatic_self_calibration(0);
}

CO2Sensor co2{};

class VOCSensor {
private:
        SemaphoreHandle_t _mutex;
        void _lock() {xSemaphoreTake(_mutex, portMAX_DELAY);}
        void _unlock() {xSemaphoreGive(_mutex);}
        // must hold mutex to access these two
        uint16_t _count = 0;
        int32_t _sum;
        bool present = false;
public:
        float value;
        bool valid = false;
        VOCSensor() {}
        void update();
        void measure();
        void init();
};

void VOCSensor::init() {
        _mutex = xSemaphoreCreateMutex();
        present = sgp40_probe() == 0;
}

void VOCSensor::update() {
        if(_count == 0) {
                valid = 0;
                return;
        }
        _lock();
        value = 32768.0f - ((float) _sum)/_count;
        valid = 1;
        _sum = 0;
        _count = 0;
        _unlock();
}

void VOCSensor::measure() {
        if(!present)
                return;
        uint16_t voc_raw;
        int16_t err = sgp40_measure_raw_with_rht_blocking_read(
                1000*temp_rh.rh, 1000*temp_rh.temperature, &voc_raw);
        if(err == NO_ERROR) {
                _lock();
                _sum += voc_raw;
                _count ++;
                _unlock();
        }
}

VOCSensor voc{};

uint32_t start_time = 0;

#define I2C_TIMEOUT   (100 / portTICK_RATE_MS)
#define BARO_ADDR 0x56

class Barometer {
private:
        SemaphoreHandle_t _mutex;
        void _lock() {xSemaphoreTake(_mutex, portMAX_DELAY);}
        void _unlock() {xSemaphoreGive(_mutex);}
        int _read(uint8_t addr, int count, uint8_t *data) {
                int rv = i2c_master_write_read_device(
                        0, BARO_ADDR, &addr, 1, data, count, I2C_TIMEOUT);
                if (rv == ESP_OK)
                        return 0;
                return -1;
        }
        int _write(uint8_t addr, uint8_t data) {
                uint8_t td[2];
                td[0] = addr;
                td[1] = data;
                int rv = i2c_master_write_to_device(0, BARO_ADDR, td, 2, I2C_TIMEOUT);
                if (rv == ESP_OK)
                        return 0;
                return -1;
        }
        int _adc_read(uint8_t addr){
                uint8_t d[3];
                _read(addr, 3, d);
                int rv = (d[0]<<16) + (d[1]<< 8) + (d[2]);
                return rv - (1<<23);
        }
        int16_t _get_coe_16(uint8_t addr){
                uint8_t d[2];
                _read(addr, 2, d);
                int16_t rv = d[1];
                rv |= ((uint16_t)d[0]) << 8;
                rv = -(rv & (1 << 15)) + (rv & ~(1<<15));
                return rv;
        }
        int32_t _get_coe_24(uint8_t addr){
                uint8_t d[3];
                _read(addr, 3, d);
                int32_t rv = d[2];
                rv |= ((uint32_t)d[1]) << 8;
                rv |= ((uint32_t)d[0]) << 16;
                rv = -(rv & (1 << 23)) + (rv & ~(1<<23));
                return rv;
        }
        float aa, ba, ap, bp, at, bt, ct;
	int32_t ca, cp;
        // must hold mutex to access these two
        float _sum_pressure = 0.0f;
        float _sum_temperature = 0.0f;
        uint16_t _count = 0;
public:
        bool present = false;
        bool valid = false;
        float pressure;
        float temperature;
        Barometer() {}
        void init();
        void measure();
        void update();
};

void Barometer::update() {
        if(_count == 0) {
                valid = 0;
                return;
        }
        _lock();
        float inv_count = 1.0f / _count;
        pressure = _sum_pressure * inv_count;
        temperature = _sum_temperature * inv_count;
        _count = 0;
        _sum_temperature = 0;
        _sum_pressure = 0;
        valid = 1;
        _unlock();
}

void Barometer::measure() {
        if(!present)
                return;
        int dp = _adc_read(0xF7);
        int dt = _adc_read(0xFA);
        float tr = ((-ba) - sqrtf(ba*ba - 4.0f*aa*(ca-dt))) / (2.0f*aa);
        float t = tr/256.0f;
        float pl = (-bp+sqrtf(bp*bp-4.0f*ap*(cp-dp)))/(2.0f*ap);
        float pressure = pl/(at*tr*tr+bt*tr+(ct+1));
        if(pressure < 40000) // block garbage data
                return;
        _lock();
        _sum_temperature += t;
        _sum_pressure += pressure;
        _count ++;
        _unlock();
}

void Barometer::init() {
        _mutex = xSemaphoreCreateMutex();
        uint8_t chipid;
        _read(0xD1, 1, &chipid);
        if(chipid != 0x5C)
                return;
        present = true;
        _write(0xF5, 0xB0);
        _write(0xF4, 0xFF);
        _write(0xF1, 0x1);
        aa = (4.2e-4f / 32767.0f) * _get_coe_16(0xB3);
        ba = -1.6e2f + (8e0f/ 32767.0f) * _get_coe_16(0xB1);
        ca = _get_coe_24(0xAD);
        ap = (3e-5f / 32767.0f) * _get_coe_16(0xA5);
        bp = 3e1f + (1e1f / 32767.0f) * _get_coe_16(0xA3);
        cp = _get_coe_24(0xA0);
        at = (8e-11f / 32767.0f) * _get_coe_16(0xAB);
        bt = -6.6e-6f + (1.6e-6f/32767.0f) * _get_coe_16(0xA9);
        ct = 4e-2f + (8.5e-3f/32767.0f) * _get_coe_16(0xA7);
}

Barometer barometer;

static esp_err_t data_get_handler(httpd_req_t *req)
{
        httpd_resp_set_type(req,  HTTPD_TYPE_JSON);
        char response[1024];
        httpd_resp_send_chunk(req, "{\n", HTTPD_RESP_USE_STRLEN);
        struct timeval tv;
        struct timezone tz;
        gettimeofday(&tv, &tz);
        if(particulate.valid){
                for(size_t i=0; i<12; i++){
                        snprintf(response, sizeof response, "  \"%s\":%.2f,\n",
                                 particulate.names[i], particulate.data_averaged[i]);
                        httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
                }
                snprintf(response, sizeof response, "  \"usepa_pm25\":%0.3f,\n", particulate.usepa_pm25);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
                snprintf(response, sizeof response, "  \"usepa_pm25_alt\":%0.9f,\n", particulate.usepa_pm25_alt);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
                httpd_resp_send_chunk(req, "  \"particulate_sample_l\":0.1,\n", HTTPD_RESP_USE_STRLEN);
        }
        if(barometer.valid) {
                snprintf(response, sizeof response, "  \"pressure\":%0.2f,\n", barometer.pressure);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
                snprintf(response, sizeof response, "  \"temperature_baro\":%0.2f,\n", barometer.temperature);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
        }
        if(voc.valid) {
                snprintf(response, sizeof response, "  \"voc_raw\":%0.2f,\n", voc.value);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
        }
        if(co2.valid) {
                snprintf(response, sizeof response, "  \"co2_ppm\":%0.2f,\n", co2.value);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
        }
        if(temp_rh.valid) {
                snprintf(response, sizeof response, "  \"temperature\":%0.2f,\n", temp_rh.temperature);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
                snprintf(response, sizeof response, "  \"relative_humidity\":%0.2f,\n", temp_rh.rh);
                httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
        }
        snprintf(response, sizeof response, "  \"time\":%d\n}\n", tv.tv_sec);
        httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
        httpd_resp_send_chunk(req, response, 0);
        return ESP_OK;
}

static const httpd_uri_t data = {
    .uri       = "/data.json",
    .method    = HTTP_GET,
    .handler   = data_get_handler,
};

static esp_err_t config_get_handler(httpd_req_t *req)
{
        char*  buf;
        size_t buf_len;
        // Read URL query string length and allocate memory for length + 1 extra byte for null termination
        buf_len = httpd_req_get_url_query_len(req) + 1;
        if (buf_len > 1) {
                buf = (char *) malloc(buf_len);
                if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
                        char param[32];
                        if (httpd_query_key_value(buf, "co2_cal", param, sizeof(param)) == ESP_OK) {
                                co2.calibrate(strtod(param, NULL));
                        }
                }
                free(buf);
        }
        char response[1024];
        snprintf(response, sizeof response, "co2_cal = %d\n", 0); // fixme
        httpd_resp_send_chunk(req, response, HTTPD_RESP_USE_STRLEN);
        httpd_resp_send_chunk(req, response, 0);
        return ESP_OK;
}

static const httpd_uri_t config_uri = {
    .uri       = "/config",
    .method    = HTTP_GET,
    .handler   = config_get_handler,
};

static esp_err_t root_handler(httpd_req_t *req)
{
    httpd_resp_send(req, "Air quality sensor - contact darrell@harmoninstruments.com /data.json contains current sensor data\n", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

static const httpd_uri_t root = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = root_handler,
};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG_HTTP, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG_HTTP, "Registering URI handlers");
        httpd_register_uri_handler(server, &data);
        httpd_register_uri_handler(server, &config_uri);
        httpd_register_uri_handler(server, &root);
        return server;
    }

    ESP_LOGI(TAG_HTTP, "Error starting server!");
    return NULL;
}

static void disconnect_handler(void* arg, esp_event_base_t event_base,
                               int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server) {
        ESP_LOGI(TAG_HTTP, "Stopping webserver");
        httpd_stop(*server);
        *server = NULL;
    }
}

static void connect_handler(void* arg, esp_event_base_t event_base,
                            int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server == NULL) {
        ESP_LOGI(TAG_HTTP, "Starting webserver");
        *server = start_webserver();
    }
}

static void i2c_task(void *pvParameters)
{
        co2.init();
        voc.init();
        barometer.init();
        temp_rh.init();
        while (1) {
                temp_rh.measure();
                co2.measure();
                barometer.measure();
                voc.measure();
                vTaskDelay(1000/portTICK_RATE_MS);
        }
}

esp_http_client_config_t config = {
        .url = INFLUX_URL,
        /*
        .cert_pem = "-----BEGIN CERTIFICATE-----\n\
MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/\n\
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n\
DkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1ow\n\
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\n\
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEB\n\
AQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XC\n\
ov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpL\n\
wYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+D\n\
LtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK\n\
4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5\n\
bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5y\n\
sR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZ\n\
Xmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4\n\
FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBc\n\
SLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2ql\n\
PRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TND\n\
TwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYw\n\
SwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1\n\
c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx\n\
+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEB\n\
ATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQu\n\
b3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9E\n\
U1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26Ztu\n\
MA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC\n\
5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW\n\
9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuG\n\
WCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9O\n\
he8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFC\n\
Dfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5\n\
-----END CERTIFICATE-----",*/
};

static void influx_upload(){
        esp_http_client_handle_t client = esp_http_client_init(&config);
        struct timeval tv;
        struct timezone tz;
        gettimeofday(&tv, &tz);

        const size_t buflen = 1280;
        char post_data[buflen];
        int len = 0;
        int rv = 0;

        rv = snprintf(post_data + len, buflen - len,
                      "airSensors,sensor_id=%s ", &hostname[11]);
        if(rv > 0)
                len += rv;
        co2.update();
        temp_rh.update();
        if(co2.valid) {
                rv = snprintf(post_data + len, buflen - len, "co2=%.2f,", co2.value);
                if(rv > 0)
                        len += rv;
        }
        if(temp_rh.valid) {
                rv = snprintf(post_data + len, buflen - len, "temperature=%.2f,humidity=%.2f,",
                              temp_rh.temperature, temp_rh.rh);
                if(rv > 0)
                        len += rv;
        }
        barometer.update();
        if(barometer.valid) {
                rv = snprintf(post_data + len, buflen - len, "pressure=%.1f,", barometer.pressure);
                if(rv > 0)
                        len += rv;
        }
        voc.update();
        if(voc.valid) {
                rv = snprintf(post_data + len, buflen - len, "voc_raw=%.2f,", voc.value);
                if(rv > 0)
                        len += rv;
        }
        particulate.update();
        if(particulate.valid) {
                rv = snprintf(post_data + len, buflen - len, "pm25=%.2f,", particulate.data_averaged[1]);
                if(rv > 0)
                        len += rv;
                uint8_t sizes[] = {3,5,10,25,50,100};
                for(size_t i=0; i<6; i++){
                        rv = snprintf(post_data + len, buflen - len,
                                      "particulate_%d=%.2f,", (int) sizes[i],
                                      particulate.data_averaged[6+i]);
                        if(rv > 0)
                                len += rv;
                }
                rv = snprintf(post_data + len, buflen - len,
                              "particulate_usepa=%.3f,", particulate.usepa_pm25);
                if(rv > 0)
                        len += rv;
                rv = snprintf(post_data + len, buflen - len,
                              "particulate_usepa_alt=%.3f,", particulate.usepa_pm25_alt);
                if(rv > 0)
                        len += rv;
        }
        rv = snprintf(post_data + len, buflen - len,
                      "uptime=%d ", tv.tv_sec - start_time);
        if(rv > 0)
                len += rv;

        rv = snprintf(post_data + len, buflen - len, "%d\n", tv.tv_sec);
        if(rv > 0)
                len += rv;
        else
                return;
        esp_http_client_set_method(client, HTTP_METHOD_POST);
        esp_http_client_set_post_field(client, post_data, strlen(post_data));
        esp_http_client_set_header(client, "Content-Type", "text/plain; charset=utf-8");
        esp_http_client_set_header(client, "Accept", "application/json");
        esp_http_client_set_header(client, "Authorization", INFLUX_KEY);
        for(int i=0; i<3; i++) {
                esp_err_t err = esp_http_client_perform(client);
                printf("upload attempt %d, %d\n%s\n", i, err, post_data);
                if (err == ESP_OK) {
                        ESP_LOGI("https", "Status = %d, content_length = %d",
                                 esp_http_client_get_status_code(client),
                                 esp_http_client_get_content_length(client));
                        break;
                }
        }
        esp_http_client_cleanup(client);
}

static void influx(void *pvParameters){
        while (1){
                printf("influx loop\n");
                vTaskDelay(10000/portTICK_RATE_MS);
                influx_upload();
        }
}

extern "C" {
        void app_main();
}

void app_main(void)
{
        ESP_ERROR_CHECK(nvs_flash_init());
        ESP_ERROR_CHECK(esp_netif_init());
        ESP_ERROR_CHECK(esp_event_loop_create_default());

        particulate.init();

        uint8_t mac[6];
        esp_read_mac(mac, ESP_MAC_WIFI_STA);
        snprintf(hostname, sizeof hostname, "airquality-%02X%02X%02X", mac[3], mac[4], mac[5]);
        if(strstr(hostname, "B27DC0") != NULL)
                snprintf(hostname, sizeof hostname, "airquality-gray");
        else if(strstr(hostname, "B27854") != NULL)
                snprintf(hostname, sizeof hostname, "airquality-black");
        else if(strstr(hostname, "B27808") != NULL)
                snprintf(hostname, sizeof hostname, "airquality-blue");
	else if(strstr(hostname, "B27E28") != NULL)
                snprintf(hostname, sizeof hostname, "airquality-drystorage");
        else if(strstr(hostname, "B27E10") != NULL)
                snprintf(hostname, sizeof hostname, "airquality-smallwhite");
        else if(strstr(hostname, "B27DF8") != NULL)
                snprintf(hostname, sizeof hostname, "airquality-smallblack");
        initialise_mdns(hostname);

        ESP_ERROR_CHECK(wifi_connect());

        i2c_config_t i2cconf {};
        i2cconf.mode = I2C_MODE_MASTER;
        i2cconf.sda_io_num = 1;
        i2cconf.sda_pullup_en = GPIO_PULLUP_ENABLE;
        i2cconf.scl_io_num = 0;
        i2cconf.scl_pullup_en = GPIO_PULLUP_ENABLE;
        i2cconf.master.clk_speed = 100000;
        i2cconf.clk_flags = 0;
        int rv = i2c_param_config(0, &i2cconf);
        ESP_ERROR_CHECK(rv);
        rv =  i2c_driver_install(0, i2cconf.mode, 0, 0, 0);
        ESP_ERROR_CHECK(rv);
        static httpd_handle_t server = NULL;
        // Register event handlers to stop the server when Wi-Fi is disconnected and re-start it upon connection.
        ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &connect_handler, &server));
        ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server));
        server = start_webserver();
        xTaskCreate(&i2c_task, "i2c_task", 16384, NULL, 5, NULL);
        sntp_setoperatingmode(SNTP_OPMODE_POLL);
        sntp_setservername(0, "pool.ntp.org");
        sntp_init();
        vTaskDelay(10000/portTICK_RATE_MS);
        struct timeval tv;
        struct timezone tz;
        gettimeofday(&tv, &tz);
        start_time = tv.tv_sec;
        xTaskCreate(&influx, "influx", 16384+65536, NULL, 5, NULL);
        particulate.task();
        while(1)
                vTaskDelay(1000/portTICK_RATE_MS);
}
