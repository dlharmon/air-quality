# air-quality

ESP32-C3 air quality monitor

# Firmware
Same firmware for full air quality sensor and temp/RH only.

### Building and flashing

[ESP IDF](https://github.com/espressif/esp-idf) is required to build the firmware

The devices can be programmed via the integrated USB debug interface.

```bash
source path-to/esp-idf/export.sh
idf.py build
idf.py -p /dev/ttyACM0 flash

```

# Software
 - [ESP32-C3 API docs](https://docs.espressif.com/projects/esp-idf/en/latest/esp32c3/api-reference/index.htm)l